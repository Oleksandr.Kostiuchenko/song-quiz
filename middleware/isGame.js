export default function (context) {
  if (!context.store.state.playerName) {
    return context.redirect('/')
  }
}
