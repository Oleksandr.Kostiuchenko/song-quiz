import {
  START_GAME,
  LOAD_QUIZ_DATA,
  SET_ANSWERS,
  UPDATE_SCORE,
  RESET_SCORE,
  SHOW_LOADING_SPINNER,
} from './-listOfMutations'

export const state = () => ({
  playerName: '',
  quizData: [],
  correctAnswers: [],
  playerScore: 0,
  showLoading: false,
})

export const mutations = {
  [START_GAME](state, playerName) {
    state.playerName = playerName
  },
  [LOAD_QUIZ_DATA](state, quizData) {
    state.quizData = quizData
  },
  [SET_ANSWERS](state, randomAnswers) {
    state.correctAnswers = randomAnswers
  },
  [UPDATE_SCORE](state, stepScore) {
    state.playerScore = state.playerScore + stepScore
  },
  [RESET_SCORE](state) {
    state.playerScore = 0
  },
  [SHOW_LOADING_SPINNER](state, payload) {
    state.showLoading = payload
  },
}

export const actions = {
  startGame({ commit }, playerName) {
    commit('START_GAME', playerName)
    commit('SHOW_LOADING_SPINNER', true)
    const randomAnswers = []
    for (let i = 0; i < 4; i++) {
      randomAnswers.push(Math.floor(Math.random() * 4))
    }
    commit('SET_ANSWERS', randomAnswers)
  },
  async fetchQuizData({ commit }) {
    try {
      const quizData = await this.$axios.$get(
        'https://levi9-song-quiz.herokuapp.com/api/data'
      )
      commit('LOAD_QUIZ_DATA', quizData)
      await this.$router.push('/quiz')
      const imageURLs = []
      quizData.map((block) => block.data.map((id) => imageURLs.push(id.image)))
      await Promise.all(
        imageURLs.map(
          async (imagePath) =>
            await this.$axios.$get(
              `https://levi9-song-quiz.herokuapp.com/api/${imagePath}`
            )
        )
      )
      const songURLs = []
      quizData.map((block) => block.data.map((id) => songURLs.push(id.audio)))
      await Promise.all(
        songURLs.map(
          async (audioPath) =>
            await this.$axios.$get(
              `https://levi9-song-quiz.herokuapp.com/api/${audioPath}`
            )
        )
      )
    } catch (error) {
      this.error = error
    } finally {
      commit('SHOW_LOADING_SPINNER', false)
    }
  },
  updateScore({ commit }, stepScore) {
    commit('UPDATE_SCORE', stepScore)
  },
  reStartGame({ commit }) {
    commit('RESET_SCORE')
    const randomAnswers = []
    for (let i = 0; i < 4; i++) {
      randomAnswers.push(Math.floor(Math.random() * 4))
    }
    commit('SET_ANSWERS', randomAnswers)
    this.$router.push('/quiz')
  },
}

export const getters = {
  currentScore: (state) => {
    return state.playerScore
  },
  correctAnswers: (state) => {
    return state.correctAnswers
  },
}
